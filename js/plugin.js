/**
 * Copyright - now till when your guilt has the better of you
 * for blatantly exploiting the labours of others. Remember
 * those who have contributed to your fame and fortune when
 * you buy that yacht.
 *
 * Table Widths - a CKEditor Plugin to set table widths to
 * 500px instead of the default 100%.
 */
(function($) {

  CKEDITOR.plugins.add('ckeditor_upload', {
    init: function(editor, pluginPath) {
      var upload_path = Drupal.settings.ckeditor_upload_path;

      editor.config.filebrowserUploadUrl = upload_path + '/file';
      editor.config.filebrowserImageUploadUrl = upload_path + '/image';
    }
  });

})(jQuery);